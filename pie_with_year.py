from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

colors = {"background": "#111111", "text": "#7FDBFF"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data/ghg-emission-of-thailand-edited-july-2021.xlsx")
df.columns = [
    "BE",
    "emission_type",
    "category",
    "sub_category",
    "en_sub_category",
    "quantity",
]
df["quantity"] = df["quantity"].replace("-", 0)
power = df[df["emission_type"].str.contains("ภาคพลังงาน")]


@app.callback(Output("pie-graph", "figure"), Input("year-slider", "value"))
def show_data(selected_year):
    filtered_df = power[power.BE == selected_year]

    fig = px.pie(
        filtered_df,
        values="quantity",
        names="sub_category",
        color="sub_category",
    )
    # fig.update_layout(transition_duration=500)

    return fig


app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Hello Dash",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: A web application framework for your data.",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id="pie-graph"),
                    ],
                    # className="col-6",
                ),
                html.Div(
                    [
                        dcc.Slider(
                            df["BE"].min(),
                            df["BE"].max(),
                            step=None,
                            value=df["BE"].min(),
                            marks={str(year): str(year) for year in df["BE"].unique()},
                            id="year-slider",
                        ),
                    ],
                    style={"padding-top": "20px;"},
                    # className="col-sm",
                ),
            ],
            className="row",
        ),
    ],
    className="container-fluid",
)

if __name__ == "__main__":
    app.run_server(debug=True)
