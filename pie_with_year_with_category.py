from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

colors = {"background": "#111111", "text": "#7FDBFF"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data/ghg-emission-of-thailand-edited-july-2021.xlsx")
df.columns = [
    "BE",
    "emission_type",
    "category",
    "sub_category",
    "en_sub_category",
    "quantity",
]
df["quantity"] = df["quantity"].replace("-", 0)
# power = df[df["emission_type"].str.contains("ภาคพลังงาน")]


@app.callback(
    Output("pie-graph", "figure"),
    Input("year", "value"),
    Input("emission_type", "value"),
)
def show_data(selected_year, emission_type):
    print(selected_year, emission_type)
    filtered_df = df[df["BE"] == int(selected_year)][
        df["emission_type"].str.contains(emission_type)
    ]
    # print(filtered_df)
    fig = px.pie(
        filtered_df,
        values="quantity",
        names="category",
        # color="sub_category",
    )
    # fig.update_layout(transition_duration=500)

    return fig


app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Hello Dash",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: A web application framework for your data.",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id="pie-graph"),
                    ],
                    className="col-8",
                ),
                html.Div(
                    [
                        # dbc.Dropdown(df["BE"].unique(), df["BE"].min(), id="year"),
                        # dbc.Dropdown(
                        #     df["emission_type"].unique(),
                        #     df["emission_type"][0],
                        #     id="emission_type",
                        # ),
                        dbc.Select(
                            options=[
                                dict(label=be, value=be) for be in df["BE"].unique()
                            ],
                            id="year",
                            value=df["BE"].min(),
                        ),
                        dbc.Select(
                            options=[
                                dict(label=et.strip(), value=et.strip())
                                for et in df["emission_type"].unique()
                            ],
                            id="emission_type",
                            value=df["emission_type"][0],
                        ),
                    ],
                    className="col-4",
                ),
            ],
            className="row",
        ),
    ],
    className="container-fluid",
)

if __name__ == "__main__":
    app.run_server(debug=True)
