from dash import Dash, dcc, html
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

colors = {"background": "#111111", "text": "#7FDBFF"}

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("data/ghg-emission-of-thailand-edited-july-2021.xlsx")
df.columns = [
    "BE",
    "emission_type",
    "category",
    "sub_category",
    "en_sub_category",
    "quantity",
]
power = df[df["emission_type"].str.contains("ภาคพลังงาน")]

fig = px.line(power, x="BE", y="quantity", color="sub_category")

fig2 = px.bar(
    power, x="BE", y="quantity", color="sub_category", orientation="v", barmode="group"
)

power1 = power[power["BE"] == 2551]
print(
    power1,
)
fig3 = px.pie(
    power1,
    values="quantity",
    names="sub_category",
    color="sub_category",
)

app.layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H1(
                    children="Hello Dash",
                    style={
                        "textAlign": "center",
                    },
                )
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    children="Dash: A web application framework for your data.",
                    style={
                        "textAlign": "center",
                    },
                ),
            ],
            className="row",
        ),
        html.Div(
            children=[
                html.Div(
                    [dcc.Graph(id="example-graph-1", figure=fig)], className="col-6"
                ),
                html.Div(
                    [dcc.Graph(id="example-graph-2", figure=fig2)], className="col-6"
                ),
            ],
            className="row",
        ),
        html.Div(
            [
                html.Div(
                    [dcc.Graph(id="example-graph-3", figure=fig3)], className="col-6"
                ),
                html.Div([], className="col-sm"),
            ],
            className="row",
        ),
    ],
    className="container-fluid",
)

if __name__ == "__main__":
    app.run_server(debug=True)
